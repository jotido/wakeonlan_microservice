FROM python:3-alpine

EXPOSE 5000

COPY requirements.txt /requirements.txt
RUN pip install -r requirements.txt

WORKDIR /srv
CMD python app.py
