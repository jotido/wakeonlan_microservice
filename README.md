# WakeOnLan Web Microservice

Deploy a [micro]service to send WOL packages to registered computers.

It is intended to be used with a reverse-proxy.

## Installation

First, target machines must be registered in src/machines_info.json using the structure of the example. It asks for:
* Machine ethernet MAC: Necessary to send the WOL MAC.
* Machine local network ip: it is used to check the machine's state. This info is convenient but not necessary since it can be tricky to set in networks with dynamic ips.

Next, the app must be set up using docker-compose:

```bash
git clone git@gitlab.com:ricargoes/wakeonlan_webservice.git
cd wakeonlan_webservice
docker-compose up -d --build
```

Once the service is up, it will accept get requests with a "machine" argument

```
http://127.0.0.1:5000/?machine=<target-machine>
```
